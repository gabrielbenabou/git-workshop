# Git Detailed

## Git (ausführlich)

### Ausgewählte Themen

Dies ist eine Seite, die dabei helfen soll sich schnell wieder an die wichtigsten Befehle/Konzepte von Git zu erinnern. Sie bietet
lediglich eine Kurzreferenz und nur ausgewählte Themen. Die Erfahrungen beruhen auf dem Tutorial “Git
Immerson”[^1], "Schneller Git-Einstieg"[^2] und dem Tutorial "Learn Git Branching" [^3]. Sollte dies nicht weiterhelfen, hilft
```bash
git help
```
z.B.
```bash
git help commit
```

### Konzepte

#### Begriffe

- repository: Datenbank mit verschiedenen Versionen der Daten

- commit: Eine Menge von Dateien die zusammen in das Repository gespeichert werden und eine bestimmte Version der Daten
bilden

- tag: Beschriftung eines commits

- branch: Ein Zweig, der vom Hauptentwicklungszweig verschieden sein kann. Es sind z.B. gewisse Dateien im Vergleich zur
Hauptversion Dateien hinzugekommen, nicht oder in modifizierter Version vorhanden.

- master: Der Branch _master_ ist der default Branch.

- HEAD : HEAD zeigt auf den aktuellen Branch

- staging area: Eine Art Zwischenablage, in der die Dateien stehen die von Git verwaltet werden sollen und bei einem commit
berücksichtigt werden

- stash: Ein Stapel (stack), auf den man Zustände der working copy speichern kann, die meist noch nicht Reif für einen commit
sind.

#### Übersicht

In Git gibt es üblicherweise mehrere lokale repositories (jeder Developer hat z.B. eine) und ein zentrales remote
repository, welches normalerweise im Internet gehostet wird.

##### Lokale repositories

Jeder Entwickler besitzt ein lokales repository und kann über dieses verschiedene Zustände des Inhalts verwalten. Dazu können
auch lokale branches erstellt werden, die für die anderen Entwickler nicht sichtbar sind. So ist ein versioniertes Arbeiten ohne
Internetverbindung möglich, da die meisten Befehle lokal stattfinden.

Begriffe: staging area, commit, lokale branches

##### Remote repositories

Gemeint sind mit remote repositories üblicherweise die zentralen repositories. Die Daten die jeder Entwickler lokal in seinem
repository verwaltet können mit diesem zentralen repository abgeglichen werden. So entsteht eine Plattform für kollaboratives,
versionisiertes Arbeiten.


Begriffe: push, pull, remote branches, stash

##### staging area

Im Dateisystem wählt man einen Ordner, der das repository darstellen soll. In diesem Befinden sich die Dateien, die versionert
werden sollen und auch solche die nicht, oder nicht jedes Mal einem commit hinzugefügt werden sollen.

Bevor man also einen commit machen kann, muss man Git zunächst mitteilen, welche Dateien/Ordner für den commit
berücksichtigt werden sollen. Diese Dateien befinden sich anschliessend in der _staging area_

#### Vorgehen

**Dateien in repository sichern**

Die vorgehensweise um eine neue Version der Daten zu speichern ist also:

- Änderungen an den gewünschten Dateien vornehmen
- Geänderte/neue Dateien in staging area aufnehmen
- Commiten (Neue Daten sind jetzt lokal gesichert)
- Pushen (Daten vom lokalen repository werden ins remote repository geladen)

**Dateien von repository abrufen**

Um neue Dateien vom remote repository abzurufen:

- Pull ausführen
- evtl. Konflikte behandeln
- Wenn Konflikt: manuell auflösen, Änderung zu staging area hinzufügen und committen

**Dateien mitten in der Arbeit von repository abrufen**

Es kommt oft vor, dass man schon einige Dateien verändert hat, wenn man pullen möchte. Ein pull würde aber die lokalen
Änderungen überschreiben. So muss man sich entscheiden, ob man die aktuelle Arbeit frühzeitig committen oder verwerfen will.
Dies ist beides suboptimal: Deshalb gibt es auch die Möglichkeit die aktuelle working copy auf den stash abzulegen und evtl. nach dem
pull wieder abzuholen.

- Benötigte Dateien in staging area aufnehmen
- Diese Dateien auf den stash laden
- Pull ausführen
- Dateien wieder vom stash laden
- evtl. Konflikte behandeln (wie bei merge)

##### Befehle

###### Setup - Name/Mail

Damit in den Commits der richtige Name und Mail-Adresse erscheint, kann dieser global definiert werden.
```bash
git config --global user.name "Name"
git config --global user.email "muster@test.com"
```

###### Neues Repository erstellen

Um ein neues Repository zu erstellen kann der Befehl _init_ verwendet werden. Dabei spielt es keine Rolle, ob bereits
Projektdateien vorhanden sind. Der Befehl sollte in dem Root-Ordner der Projektdateien aufgeführt werden (sofern alle
Projektdateien nur in ein Repository hinzugefügt werden sollen).
```bash
git init
```
###### Dateien hinzufügen

Um Dateien zum Repository hinzuzufügen sind zwei Schritte nötig: Man muss zunächst die Datei in die _Staging Area_
hinzufügen. Danach kann dieser mit einer Meldung in das Repository geschrieben werden. Nur die Dateien die _staged_ sind,
werden in den Commit aufgenommen. Wenn die Option _-m_ weggelassen wird, wird der Standard-Texteditor gestartet indem man
die Commit-Meldung verfassen kann.
```bash
# FILE in staged area aufnehmen
git add <FILE>

# staged area speichern (committen)
git commit -m "COMMIT_MESSAGE"
```
###### Staging Area leeren
```bash
# Setzt die Staging Area auf das was in HEAD ist
git reset HEAD [datei]
```
Reset verändert die working copy nicht! Dazu müsste ein _checkout_ vorgenommen werden.
```bash
git add file.py
git commit -m "Meine Commit Nachricht"
```
###### Aktueller Repository-Zustand abfragen

Dies ist sehr nützlich um zu überprüfen, ob Änderungen noch nicht hinzugefügt oder committed wurden.
```bash
git status
```
###### Änderungen speichern

Dies ist sehr schnell erklärt: Änderungen an einer Datei können genau gleich behandelt werden, wie wenn die Datei neu
hinzugefügt würde. Der einzige Unterschied besteht darin, dass das Repository (vor dem hinzufügen) die Datei bereits kennt und
als geändert erkannt hat (kann mittels _git status_ eingesehen werden).
```bash
# FILE in staging area aufnehmen
git add <FILE>

# staged area speichern (committen)
git commit -m "COMMIT_MESSAGE"
```

###### Änderung an commit anfügen

Um eine Änderung nachträglich noch in den letzten commit mitaufzunehmen kann man folgendes tun:
```bash
git add <vergessene_Datei>
git commit --amend -m "<Kommentar>"
```
###### Log ansehen

Im Log stehen alle Commits mit den Meldungen und weitere Informationen. Die Ausgabe kann verschieden formatiert werden:
```bash
# Ausführliches Log
git log

# Log mit allen Branches
git log --all

# Kompaktes Log (übersichtlich)
git log --pretty=oneline

# Grafische Darstellung der Branches
git log --all --graph

# Selbstformatiertes Log
git log --pretty=format:'%h %ad | %s%d [%an]' --graph --date=short
```

###### Zu bestimmter Version wechseln

Zu einer bestimmten Version kann man mittels des Befehls "checkout" wechseln. Entweder mittels eines Hashes eines Commits
oder durch den Namen eines Branches. Vorher sollte man ggf. Änderungen im aktuellen Branch committen oder stashen. _git
checkout_ kann auch verwendet werden, um alte Versionen einer Datei wiederherzustellen in dem man
```bash
git checkout <hash> -- [datei]
```
benutzt.

**mittels Hash**

Als nächstes muss man den Hash des Commits herausfinden (z.B. über den Befehl log).
```bash
git checkout <hash>
```
**mittels Tag**

Zu einem Commit (oder dessen Parent) kann auch mittels Tag gewechselt werden.
```bash
# wechselt zum Commit mit dem Tag 'v1'
git checkout v1

# wechselt zum Commit vor dem Commit mit dem Tag 'v1'
git checkout v1^
```
###### Taggen

Um einen Commit zu taggen kann der folgende Befehl verwendet werden:
```bash
git tag <TAG_NAME>
```
Damit wird einem Commit ein Tag zugeordnet. Über diesen kann man den Commit identifizieren (neben dem Hashwert)

###### Tags auflisten
```bash
git tag
```

###### Tags entfernen
```bash
git tag -d <TAG_NAME>
```
###### Änderungen rückgängig machen

**mittels neuem Revert-Commit**

Mit dem Befehl _revert_ lässt sich ein neuer Commit erstellen, der die Änderungen des angegebenen Commits rückgängig macht.
```bash
# Beliebigen commit mittels Hash rückgängig machen
git revert <HASH>

# letzten commit rückgängig machen
git revert HEAD
```
###### Commits entfernen

Es gibt die möglichkeit commits zu entfernen. 
```bash
# setzt den Zustand auf den gewählten commit zurück
# --hard setzt auch die working copy auf den neuen HEAD
git reset --hard <hash | tag>

# Namen/E-Mail Adresse des letzten commits ändern
git commit --amend --author "Vorname Name <E-Mail>"
```
###### Dateien verschieben

Am besten verschiebt man Dateien direkt mit dem Git-Befehl:
```bash
# verschiebt 'file' in 'directory'
git mv [datei] [pfad]
```
Da man aber oft erst vor einem ’commit’ mit Git interagiert und deshalb die Dateien bereits mittels `mv`
verschoben hat, gibt es eine Möglichkeit Git im Nachhinein zu informieren, dass eine Datei verschoben wurde:
```bash
# 'file' wurde mittels 'mv' Befehl verschoben
mv [datei] [pfad]

# neuer Pfad der Datei zu git hinzufügen
git add [pfad]/[datei]

# alter Pfad der Datei von git entfernen
git rm [datei]
```
Beide Methoden führen zum gleichen Ergebnis. Die Datei wird im alten Pfad entfernt und mit dem neuen Pfad hinzugefügt.

###### Branch erstellen

Branch erstellen und zu diesem wechseln:
```bash
git checkout -b <branch_name>

# wenn ein remote branch verknüpft werden soll
git --track <local_branch_name> origin/<remote_branch_name>
```
Nur neuen branch erstellen:
```bash
git branch <branch_name>

# wenn man später in den branch wechseln will
git checkout <branch_name>
```
###### Branches anzeigen

```bash
# lokale branches anzeigen
git branch

# auch remote branches anzeigen
git branch -a
```
###### Branch wechseln
```bash
git checkout <branch_name>
```
###### Branches zusammenführen

Die Änderungen können von einem anderen Branch in den aktuellen Branch gemerged werden. Dazu dient dieser Befehl:
```bash
git merge <branch_name>
```

###### Konflikte

Beim Zusammenführen können Konflikte auftreten, wenn an der gleichen Dateien Änderungen vorgenommen wurden. Der
Merge-Vorgang bricht ab und es wird angezeigt, welche Dateien vom Konflikt betroffen sind. Diese Dateien enthalten dann
jeweils beide Versionen der Dateien an den jeweiligen Stellen, wo sich die Dateien unterscheiden.
```bash
<====== <branch_name1>

# Code der zu Branch 1 gehört

===========

# Code der zu Branch 2 gehört

=======> <branch_name2>
```
Um den Konflikt aufzulösen, müssen diese Stellen bereinigt werden. Danach kann wie gewohnt committed werden.

###### Stash - Working copy speichern

Dateien die auf den stash geladen werden sollen müssen zuerst in die staging area aufgenommen werden. Danach können diese
mit _git stash_ auf den stash geladen werden. Am Besten gibt man noch einen Namen an, damit man den hochgeladenen Zustand
der working copy später noch von anderen gespeicherten Zuständen unterscheiden kann.
```bash
# speichert die Dateien in der staged area auf den stash
git stash
# noch besser mit Namen
git stash save [name]

# stash anzeigen
git stash list

# Dateien von stash herunterladen (stash bleibt erhalten)
git stash apply <stash_id>
# Dateien von stash löschen
git stash drop <stash_id>
# oder beides in einem Schritt
git pop

# diff der Dateien auf dem stash anzeigen


git stash show -p <stash_id>
```
###### Repository klonen
```bash
git clone <existing_repo_name> <new_repo_name>

# oder mit gleichem Namen klonen (remote Repository)
git clone <gilab_clone_url>
```
###### Remote Repository

**Informationen anzeigen**
```bash
# Standardmässig heisst das remote repository 'origin'
git remote show origin
```
**Änderungen herunterladen**
```bash
git fetch
```
Dies lädt die Änderungen des remote repository runter, merged diese nicht in den lokalen branch. Eigentlich
nur commit Informationen heruntergeladen.

Um die inhaltlichen Änderungen zu bekommen, kann nun der remote branch in den aktuellen branch gemerged werden:
```bash
git merge origin/master
```
Es gibt einen einfacheren Weg, direkt die remote vorliegenden Änderungen und den
lokalen Zustand zusammenzuführen:
```bash
git pull
```

**Änderungen hochladen**
```bash
git push <remote_repository_name> <remote_branch_name>
```

[^1]: Web-Seite: [http://gitimmersion.com/](http://gitimmersion.com/)
[^2]: Web-Seite: [https://learngitbranching.js.org/?locale=de_DE](https://learngitbranching.js.org/?locale=de_DE)
[^3]: Web-Seite: [https://t3n.de/news/schneller-git-einstieg-befehle-1077761/](https://t3n.de/news/schneller-git-einstieg-befehle-1077761/)