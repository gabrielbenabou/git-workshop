# Git Workshop Aufgabe

## Einführung
Für alle fleissigen AnyWebber, die Zeit und Interesse haben, wurde eine kleine Aufgabenstellung vorbereitet. Die Lösungen sind in "Git Workshop Lösungen" zu finden.

## Aufgabe 1 - GUI-Based: (Schwierigkeitsgrad: **einfach**)
1. Sie sollen unter [https://gitlab.com/users/sign_up](https://gitlab.com/users/sign_up) registrieren und ein eigenes Projekt (Repository) erstellen. Das Repository sollte inkl. README erzeugt werden.
2. Lernen Sie das GUI kennen und erzeugen Sie einen neuen Branch, indem Sie auf das [+] Symbol klicken. Er soll den Namen _development_ haben.
3. Wechseln Sie in Ihren neuen _development_ Branch und fügen Sie diesmal eine Datei anstelle eines Branches hinzu.
4. Mit dem Web-IDE können Sie die Datei bearbeiten und committen.

## Aufgabe 2 - GUI-Based & Command-Line: (Schwierigkeitsgrad: **mittel**)
1. Sie sollen unter [https://gitlab.com/users/sign_up](https://gitlab.com/users/sign_up) registrieren und ein eigenes Projekt (Repository) erstellen. Das Repository sollte inkl. README erzeugt werden.
2. Klonen Sie das Repository in Ihr Working Directory.
3. Erzeugen Sie einen Branch _development_.
4. Wechseln Sie den Branch von _master_ auf _development_.
5. Erzeugen Sie eine Datei `test.md` mit folgenden Inhalt:
```
# Mein Titel
Dies ist mein erstes MarkDown-File
```
6. Stagen Sie nun die neue Datei.
7. Committen Sie die Datei.
8. Pushen Sie alle Änderungen in das Remote Repository.
9. Mergen Sie nun den Branch _development_ in den _master_ Branch indem Sie unter _Merge Requests > New merge request_ einen Zusammenführeng verlangen.
10. Die datei `test.md` sollte nun auch im _master_ Branch vorliegen. Klicken Sie drauf und schauen Sie sich das dargestellte MarkDown-File an.

## Aufgabe 3 - AnyWeb Projekte: (Schwierigkeitsgrad: **einfach**)
Gratuliation, dass Sie es bis hier hin geschafft haben! Für die nächste Aufgabe müssen Sie Thomas Lanz oder Philippe Naegeli kontaktieren, um in die Gruppe "anyweb" aufgenommen zu werden.
Dies erlaubt Ihnen einen spannenden Einblick in die aktuellen Projekte.

1. Klonen Sie das Repository _anyweb1/git-workshop_ in Ihr Working-Directory.
2. Wechseln Sie in den _development_ Branch.
3. Erzeugen Sie einen eigenen Branch _name-vorname_ und wechseln Sie in diesen.
4. Öffnen Sie die Datei "README.md" und fügen Sie in der Liste Ihren Namen hinzu.
5. Committen Sie die Änderung.
6. Pushen Sie die Änderung.
7. Mergen Sie über's GUI Ihren Branch in _development_ wählen Sie auch die Option "[X] Delete source branch when merge request is accepted." aus.

Das war's! Wenn Sie die Übungen erfolgreich (hoffentlich ohne zu spicken 😉) absolviert haben, danken wir Ihnen für die tolle Mitarbeit.
