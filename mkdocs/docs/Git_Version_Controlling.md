# Version Control

## Version Controlling

Versionierung von mehreren Fileversionen in einem Repository. Eine Bearbeitung des Files bedingt eine lokale **working copy**. 

Einige Vorteile von Version Control: 

Funktion | Vorteil
----|----
Kollaborationsmöglichkeiten | Mehrere Personen können gleichzeitig an einem Projekt arbeiten 
Rechenschaftspflicht und Sichtbarkeit | Wissen, wer welche Änderungen vorgenommen hat, wann und (commit) warum. 
Isolation der produktiven/devolopment/feature Branches | Neue Funktionen unabhängig voneinander erstellen, ohne die vorhandene Software zu beeinträchtigen. 
Sichern und Wiederherstellen | Änderungen an Dateien können durch Versionierung zurückgespielt werden.  

### Lokales Version Controlling 

Man muss keine Kopie der Datei vor der Bearbeitung erstellen. In einer Datenbank werden meistens die Versionsdeltas erfasst. 
![LVC](src/img/lvc.png)

### Zentrales Version Controlling

Dateien müssen von einem Server **ausgecheckt** (Sperrt die Dateibearbeitung) und lokal abgespeichert werden. Bei einem **Einchecken** wird auf dem Server eine neue Version kreiert, die Kopie abgelegt und die Bearbeitung freigeschaltet. 
![ZVC](src/img/zvc.png)

### Verteiltes (Dezentralisiertes) Version Controlling 

Jeder hat eine Repository-Kopie lokal abgelegt und kann alle Dateien bearbeiten. Bei einem **Push** werden Änderungen zusammengeführt usw. Eine Sperrung der Dateien fällt weg. 

![VVC](src/img/vvc.png)