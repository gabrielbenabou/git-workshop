# Cheat Sheet der Kommandos


Kommando                    | Beschreibung
----------------------------| -------------- 
`git add .`                           | Alle veränderten Dateien der Staging-Area hinzufügen. 
`git add [pfad/datei]`j                | Fügt eine Datei der Staging-Area hinzu, die später mittels commit das Repo aktualisiert.  
`git branch –d [name]`                | Löscht Branch. 
`git branch [parent] [name]`          | Erzeugt einen Branch, ohne Parameter zeigt er die aktuellen Branches an. 
`git checkout -- [datei]`             | Löscht Dateiänderungen
`git checkout –b [parent] [name]`     | Erzeugt (git branch [name]) und wechselt (git checkout [name]) working-directory in Branch.
`git clone [repository] [target directory]` | Klont ein Repo und erzeugt einen lokalen "fork" Branch des Masters 
`git commit –-all –m "msg"`           | Lokales Repo mit den allen Änderungen der Staging-Area aktualisieren. 
`git config --global key value`       | Globale Einstellungen ("user.name"/"user.email") für Accountability 
`git config alias.lg "log --oneline --graph --decorate" <br> git lg`   | Bessere Übersicht von git log 
`git diff [datei]`                     | Unterschiede zwischen beiden Dateiversionen (parent, lokal) 
`git fetch`                           | Branch-Liste aktualisieren 
`git init [pfad]`                | Erzeugt ein Repository im Ordner  
`git merge [src Branch]`              | Branches zusammenführen. 
`git pull [origin master...]`         | Lokales Repository (mit remote Repo-Änderungen) aktualisieren. Fetch und merge. 
`git push [origin master...]`         | Remote Repository aktualisieren. Origin gibt den zu aktualisierenden Branch an. 
`git remote add [name z.B. origin] [Repo-Link […].git]` | Fügt ein remote Repo hinzu 
`git reset --hard`                    | Alle Änderungen verwerfen
`git reset [pfad/datei]`              | Aus Staging-Area (Index) entfernen 
`git revert [commit]`                 | Zu einem Commit zurückkehren 
`git rm [pfad/datei]`                 | Enternt Datei vom Working Directory und Änderung der Staging-Area hinzufügen. 
`git status`                          | Gibt Infos zurück: unterschiede zwischen working directory und parent Branch, aktuelle Branch, commits 
`git switch [Branch name]`            | Wechselt working-directory zum Branch und fetch. 