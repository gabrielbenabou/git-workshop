# Git Workshop Lösungen
Hier werden vor allem nochmals die Befehle aufgelistet.

## Aufgabe 1 - GUI-Based: (Schwierigkeitsgrad: **einfach**)
Individuelle Erkenntnisse.

## Aufgabe 2 - GUI-Based & Command-Line: (Schwierigkeitsgrad: **mittel**)
Sie sollen unter [https://gitlab.com/users/sign_up](https://gitlab.com/users/sign_up) registrieren und ein eigenes Projekt (Repository) erstellen. Das Repository sollte inkl. README erzeugt werden.

``` bash
# Ins Working Directory wechseln.
cd myWorkDir
# Repo klonen
git clone [CLONE-Link im REPO]
# Neuen Branch anlegen und dahin wechseln.
git checkout -b development
# Alternativ kann folgendes verwendet werden:
# git branch master development
# git switch development

# Datei Anlegen
echo "# Mein Titel" > test.md
echo "Dies ist mein erstes MarkDown-File" >> test.md

# Stagen
git add test.md
# Comitten
git commit -m "Datei angelegt"
# Pushen
git push origin master development
```
Der Merge erfolgt im GUI.


## Aufgabe 3 - AnyWeb Projekte:
``` bash
# Ins Working Directory wechseln.
cd myWorkDir
# Repo klonen
git clone [CLONE-Link im REPO]
# Nach development wechseln.
git checkout development
# Neuen Branch erzeugen und wechseln
git checkout -n vorname-nachname

# Datei editieren mit vim, nano, notepad...

# Anschliessend muss die Datei gestaged werden. (Danke Jonas für den Input)
git add README.md

# Comitten
git commit -m "Name hinzugefügt"
# Pushen
git push origin master development vorname-nachname
```
6. Mergen Sie über's GUI Ihren Branch in _development_ wählen Sie auch die Option "[X] Delete source branch when merge request is accepted." aus.

