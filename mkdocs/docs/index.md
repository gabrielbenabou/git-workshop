# Willkommen
Dieses Wiki wurde von mir erzeugt, um mein Know-How zu vermitteln.

Bei Fehlinformationen oder Anmerkungen, können Sie gerne auf meine Kollegen aus dem ITSM-Team oder mich zukommen.

**Gabriel Ben Abou**  
ITSM Network Engineer @ AnyWeb AG,  
Hofwiesenstrasse 350, 8050 Zürich  
Kontaktieren Sie mich über:  
- [Webex](https://anyweb.webex.com/meet/gabriel.benabou)  
- [Mail](mailto:gabriel.benabou@anyweb.ch)  
- [Web](http://www.anyweb.ch)



_Autor: Gabriel Ben Abou_
_Version: 1.2_