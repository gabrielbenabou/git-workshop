# Beispiel-Workflow (mit Git-Bash)

Ein relativ einfacher Workflow besteht darin, dass man ein offizielles zentrales Repository hat. Der _master_ -Branch sollte dabei
immer eine lauffähige Version der Software sein (also quasi "stabil"). Arbeiten an einem neuen Feature
sollten möglichst in anderen Branches stattfinden.

Als erstes erstellen wir einen _dev_ -Branch (es wird gebranched vom master):
```bash
git checkout -b development
```
Anschliessend möchten wir zwei neue Features einbauen, _feature-a_ und _feature-b_. Für diese erstellen wir auch jeweils branches
(es wird gebranched von development):
```bash
git checkout development
git checkout -b feature-a
git checkout-development
git checkout -b feature-b
```
Nun können wir diese Branches ins zentrale Repository pushen:
```bash
git push origin development master feature-a feature-b
```
Dann arbeiten wir an feature-b
```bash
git checkout feature-b
echo B > B.txt
git add B.txt
git commit -m "+ B"
```
und anschliessend an feature-a
```bash
git checkout feature-a
echo A > A.txt
git add A.txt
git commit -m "+ A"
```
Nun mergen wir diese Änderungen in den development-Branch:
```bash
git checkout development
git merge feature-a
git merge feature-b
```
und pushen wieder:
```bash
git push origin development master feature-a feature-b
```
Somit haben wir alle Änderungen von feature-a und feature-b im development-Branch. Die feature-Branches können daher nun eigentlich
gelöscht werden.
```bash
git branch -d feature-a
git branch -d feature-b
git push origin :feature-a :feature-b
```
Wenn wir uns sicher sind, dass wir alle nötigen Features für eine stabile Version haben (also, dass development nun  "released"
werden kann) mergen wir den development in den master:
```bash
git checkout master
git merge development
git push origin development master
```





