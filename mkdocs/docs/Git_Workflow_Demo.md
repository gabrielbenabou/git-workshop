# Beispiel-Workflow (mit GitLab)

Ein relativ einfacher Workflow besteht darin, dass man ein offizielles zentrales Repository hat. Der _master_ -Branch sollte dabei
immer eine lauffähige Version der Software sein (also quasi "stabil"). Arbeiten an einem neuen Feature
sollten möglichst in anderen Branches stattfinden.

1. GitLab neues Projekt anlegen. (mit README)
2. Git Bash aufrufen
```bash 
cd Desktop/workspace
ls -la
git clone [REPO]
cd git-demo
ls -la
git branch
```
3. Als erstes erstellen wir einen _dev_ -Branch (es wird gebranched vom master):  
```bash 
git checkout -b development
git branch
```  
4. Team weist mir Issue für "Feature implementation" zu Issues -> Board -> Issue -> Assign
5. Verweis auf Issue ID
6. Anschliessend möchten wir ein neues Feature einbauen, _1-feature_. Für dieses erstellen wir auch jeweils branches
(es wird gebranched von development):
```bash
git checkout development
git checkout -b 1-feature
git push
git push --set-upstream origin 1-feature
```  
7. Feature 1 Issue (related Branch) erfüllen.  
```bash
echo "Done" > A.txt
ls -la
git status
git add A.txt
git commit -m"Closes #1, Feature implementiert"
```  
8. Nun mergen wir diese Änderungen in den development-Branch:  
```bash
git checkout development
git merge 1-feature
```
und pushen wieder:
```bash
git push 
git push --set-upstream origin development
git log
```  
9. Es fehlt ein review und merge durch das Team -> Gui Merge-Request  

