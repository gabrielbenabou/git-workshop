# Git Summary

## Git Flow 

Da Git ein DVCS (distributed version control system) ist, besitzt jeder Client eine **Repo-Kopie**. Im .git-Ordner befinden sich die Dateien (komprimiert, commits und logs (commit history). Das **Working-Directory** ist der sichtbare Projekt-Ordner. Dateiänderungen liegen auf der **Staging-Area**.

Status      | Was ist passier?
------------|----------
modified    | Dateiänderung liegt im Workingdirectory 
staged      | Die modifizierte Datei ist bereit für einen commit ins Repo 
committed   | Die Version der Datei wurde ins Repo aufgenommen 

![Flow](src/img/git_flow.png)


## Branching 

Branching erlaubt es Nutzern unabhängig am Teilen des Codes zu arbeiten, ohne den Master-Code zu beeinflussen. Anfangs wird alles in die _master_ Branch abgelegt. Es entstehen so verschiedene Flow-Level, die verschiedene Ablauflevel (production, development, features usw.) ermöglichen.  

![Branching](src/img/branching.png)

Wenn eine Implementierung doch schief gehen soll, kann der Branch einfach gelöscht werden. Eine Zusammenführung (merge) mit dem parent Branch ist nicht zwingend notwendig.  

Branches werden oft als separate Pfade gezeichnet, allerdings verweist git schlussendlich nur auf den korrekten commit (Pointer).